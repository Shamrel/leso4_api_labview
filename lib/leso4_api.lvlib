﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="15008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">352354304</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="dll" Type="Folder">
		<Item Name="libLESO4.dll" Type="Document" URL="../dll/libLESO4.dll"/>
	</Item>
	<Item Name="subVI" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="DisableChannel.vi" Type="VI" URL="../subVI/DisableChannel.vi"/>
		<Item Name="enable_multiple.vi" Type="VI" URL="../subVI/enable_multiple.vi"/>
		<Item Name="enable_single.vi" Type="VI" URL="../subVI/enable_single.vi"/>
		<Item Name="EnableChannel.vi" Type="VI" URL="../subVI/EnableChannel.vi"/>
		<Item Name="error.vi" Type="VI" URL="../subVI/error.vi"/>
		<Item Name="read_multiple.vi" Type="VI" URL="../subVI/read_multiple.vi"/>
		<Item Name="read_single.vi" Type="VI" URL="../subVI/read_single.vi"/>
		<Item Name="setV_multiple.vi" Type="VI" URL="../subVI/setV_multiple.vi"/>
		<Item Name="setV_single.vi" Type="VI" URL="../subVI/setV_single.vi"/>
		<Item Name="setM_single.vi" Type="VI" URL="../subVI/setM_single.vi"/>
		<Item Name="setM_multiple.vi" Type="VI" URL="../subVI/setM_multiple.vi"/>
		<Item Name="ResetFTDI.vi" Type="VI" URL="../subVI/ResetFTDI.vi"/>
	</Item>
	<Item Name="LESO4.2_open.vi" Type="VI" URL="../LESO4.2_open.vi"/>
	<Item Name="LESO4.2_enable.vi" Type="VI" URL="../LESO4.2_enable.vi"/>
	<Item Name="LESO4.2_close.vi" Type="VI" URL="../LESO4.2_close.vi"/>
	<Item Name="LESO4.2_read.vi" Type="VI" URL="../LESO4.2_read.vi"/>
	<Item Name="LESO4.2_setV.vi" Type="VI" URL="../LESO4.2_setV.vi"/>
	<Item Name="LESO4.2_setF.vi" Type="VI" URL="../LESO4.2_setF.vi"/>
	<Item Name="LESO4.2_setN.vi" Type="VI" URL="../LESO4.2_setN.vi"/>
	<Item Name="LESO4.2_setM.vi" Type="VI" URL="../LESO4.2_setM.vi"/>
	<Item Name="LESO4.2_reset.vi" Type="VI" URL="../LESO4.2_reset.vi"/>
</Library>
